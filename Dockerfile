ARG BINARY_NAME

############################
# STEP 1 build executable binary
############################
FROM golang:1.18.2@sha256:04fab5aaf4fc18c40379924674491d988af3d9e97487472e674d0b5fd837dfac AS builder

ARG BINARY_NAME
WORKDIR ${GOPATH}/src/${BINARY_NAME}

RUN apt-get install -y --no-install-recommends git ca-certificates

COPY . .

ENV CI=true
RUN make build

############################
# STEP 2 build a small image
############################
FROM alpine:3.16.0@sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c

RUN apk --no-cache update && \
    apk --no-cache add curl && \
    adduser -D -H appuser && \
    mkdir /app

WORKDIR /app

ARG BINARY_NAME
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/${BINARY_NAME}/${BINARY_NAME} /app/

RUN ln -s /app/${BINARY_NAME} /app/app && \
    chown appuser:appuser -R /app

USER appuser

HEALTHCHECK --interval=5s \
            --timeout=5s \
            --start-period=5s \
            CMD curl -f http://0.0.0.0:4242/status || exit 1

EXPOSE 4242

ENTRYPOINT ["./app"]
