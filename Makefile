build:
	GOOS=linux GOARCH=386 GO386=softfloat go build -v -o testapp

build-image:
	docker build --build-arg BINARY_NAME=testapp -t testapp .

