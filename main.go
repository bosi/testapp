package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	log.Println("start app")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "hello world")
	})

	http.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "ok")
	})

	log.Fatalln(http.ListenAndServe(":4242", nil))
}
